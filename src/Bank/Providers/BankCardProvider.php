<?php

namespace PaymentKit\Bank\Providers;

use Illuminate\Support\ServiceProvider;
use PaymentKit\Bank\Engines\AlipayBankCard;

class BankCardProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('BankCard', function () {
            return new AlipayBankCard();
        });
    }
}