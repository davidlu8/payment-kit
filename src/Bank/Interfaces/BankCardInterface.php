<?php

namespace PaymentKit\Bank\Interfaces;

use PaymentKit\Exceptions\InputException;

interface BankCardInterface
{
    /**
     * @param string $cardNo
     * @return mixed
     * @throws InputException
     */
    public function getInfo($cardNo);

    /**
     * @param $cardType
     * @return string|null
     */
    public function getCardTypeName($cardType);

    /**
     * @param $cardBankCode
     * @return string|null
     */
    public function getCardBankName($cardBankCode);

}