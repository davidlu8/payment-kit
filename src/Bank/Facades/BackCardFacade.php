<?php


namespace PaymentKit\Bank\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class BackCardFacade
 * @package PaymentKit\Bank\Facades
 *
 * @method static null|array getInfo(string $cardNo)
 * @method static null|string getCardTypeName(string $cardType)
 * @method static null|string getCardBankName(string $cardBankCode)
 *
 * @see \PaymentKit\Bank\Interfaces\BankCardInterface
 */
class BackCardFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'BankCard';
    }

}