<?php

namespace PaymentKit\Tests\Bank\Engines;

use PaymentKit\Bank\Engines\AlipayBankCard;
use PaymentKit\Exceptions\RemoteException;
use PaymentKit\Tests\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class AlipayBankCardTest extends TestCase
{
    public function dpGetInfoSceneGetSuccessCardInfoAssertData()
    {
        return [
            ['6225000011112222', json_encode([
                "cardType" => "DC",
                "bank" => "CMB",
                "key" => "6225000011112222",
                "messages" => [],
                "validated" => true,
                "stat" => "ok"
            ]), [
                'card_no' => '6225000011112222',
                'card_type' => 'DC',
                'card_bank_code' => 'CMB',
            ]],
            ['6217333322221111001', json_encode([
                "cardType" => "DC",
                "bank" => "CCB",
                "key" => "6217333322221111001",
                "messages" => [],
                "validated" => true,
                "stat" => "ok"
            ]), [
                'card_no' => '6217333322221111001',
                'card_type' => 'DC',
                'card_bank_code' => 'CCB',
            ]]
        ];
    }

    /**
     * @param $cardNo
     * @param $apiData
     * @param $excepted
     * @throws RemoteException
     * @dataProvider dpGetInfoSceneGetSuccessCardInfoAssertData
     */
    public function testGetInfoSceneGetSuccessCardInfoAssertData($cardNo, $apiData, $excepted)
    {
        /** @var AlipayBankCard|MockObject $alipayBankCard */
        $alipayBankCard = $this->getMockBuilder(AlipayBankCard::class)
            ->setMethods(['getRemoteUrl'])
            ->getMock();
        $alipayBankCard->method('getRemoteUrl')
            ->willReturn($apiData);
        $actual = $alipayBankCard->getInfo($cardNo);
        $this->assertEquals($excepted, $actual);
    }

    public function dpGetInfoSceneGetSuccessCardInfoAssertFalse()
    {
        return [
            ['6225000011112222', json_encode([
                "key" => "6225000011112222",
                "messages" => [
                    'errorCodes' => 'PARAM_ILLEGAL',
                    'name' => 'cardNo'
                ],
                "validated" => false,
                "stat" => "ok"
            ]), false],
            ['6217333322221111001', json_encode([
                "key" => "6217333322221111001",
                "messages" => [
                    'errorCodes' => 'CARD_BIN_NOT_MATCH',
                    'name' => 'cardNo'
                ],
                "validated" => false,
                "stat" => "ok"
            ]), false]
        ];
    }

    /**
     * @param $cardNo
     * @param $apiData
     * @param $excepted
     * @throws RemoteException
     * @dataProvider dpGetInfoSceneGetSuccessCardInfoAssertFalse
     */
    public function testGetInfoSceneGetSuccessCardInfoAssertFalse($cardNo, $apiData, $excepted)
    {
        /** @var AlipayBankCard|MockObject $alipayBankCard */
        $alipayBankCard = $this->getMockBuilder(AlipayBankCard::class)
            ->setMethods(['getRemoteUrl'])
            ->getMock();
        $alipayBankCard->method('getRemoteUrl')
            ->willReturn($apiData);
        $actual = $alipayBankCard->getInfo($cardNo);
        $this->assertEquals($excepted, $actual);
    }
}